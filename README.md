# hog-engine

> Hidden object game engine - Project started in my spare time to develop simple game engine for making Hidden object games.
> Developed as SPA built with vue.js (+ vue-router + Vuex), PIXI.js for canvas manipulation and Firebase for data storage and hosting.

> work in progress version: https://hog-engine-f691d.firebaseapp.com

(work in progress)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
