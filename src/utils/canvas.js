/* eslint-disable */ 
import * as PIXI from 'pixi.js'
import { Howl, Howler } from 'howler'
export default class Canvas {
  constructor(el, width, height) {
    this.background = null
    this.app = new PIXI.Application({ width, height })
    this.loader = this.app.loader
    this.clickSound = new Howl({
      src: ['https://firebasestorage.googleapis.com/v0/b/hog-engine-f691d.appspot.com/o/Click.wav?alt=media&token=5ff894c7-aba4-4fcb-8d42-4dac73276c45']
    });
    el.appendChild(this.app.view)
  }

  setBackground(background) {
    background = PIXI.Sprite.fromImage(background)
    background.width = this.app.screen.width
    background.height = this.app.screen.height
    this.app.stage.addChild(background)
    this.app.stage.setChildIndex(background, 0)
    this.background = background
  }

  addSprites(sprites) {
    let orderedSprites = this.sortSpritesByOrder(sprites)
    let spriteImages = orderedSprites.map(sprite => sprite.image)
    spriteImages = spriteImages.filter(sprite => !this.loader.resources[sprite])
    this.loader.add(spriteImages)
      .load((loader, resources) => {
        for (const key in resources) {
          let sprite = new PIXI.Sprite(resources[key].texture)
          let currentSprite = sprites.find(item => item.image === key)
          if (currentSprite && currentSprite.position) {
            let spriteId = currentSprite.spriteId
            let { x, y } = currentSprite.position
            sprite.$spriteId = spriteId
            sprite.$name = currentSprite.name
            sprite.x = x
            sprite.y = y
            sprite.width = sprite.width / 2
            sprite.height = sprite.height / 2
            sprite.interactive = true
            sprite.anchor.set(0.5)
            this.app.stage.addChild(sprite)
            sprite.on('pointerdown', () => {
              this.onSpriteClick(sprite)
            })
          }
          if (this.background) {
            this.app.stage.setChildIndex(this.background, 0)
          }
        }
      })
  }

  sortSpritesByOrder(sprites) {
    return sprites.sort((a, b) => a.order - b.order)
  }

  onSpriteClick(sprite) {
    // fadeout with scale up
    this.clickSound.play()
    let timer = 0
    let interval = setInterval(() => {
      sprite.scale.set(sprite.scale.x + 0.04)
      sprite.alpha -= 0.03
      timer += 10
      if (timer >= 500) {
        sprite.interactive = false
        clearTimeout(interval)
      }
    }, 10)
  }

}
