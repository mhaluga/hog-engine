// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router/router'
import BootstrapVue from 'bootstrap-vue'
import VueI18n from 'vue-i18n'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import messages from './translations/translations'
import firebase from 'firebase'
import { store } from './store/store'
import Icon from 'vue-awesome/components/Icon'
import Spinner from './components/Spinner.vue'
import 'vue-awesome/icons/spinner'
import 'vue-awesome/icons/window-close'
import 'vue-awesome/icons/ban'
import 'vue-awesome/icons/trash'
import 'vue-awesome/icons/eye'
import 'vue-awesome/icons/clone'
import 'vue-awesome/icons/play'
Vue.use(BootstrapVue)
Vue.config.productionTip = false
Vue.use(VueI18n)

Vue.component('icon', Icon)
Vue.component('Spinner', Spinner)

const i18n = new VueI18n({
  locale: 'en',
  messages
})

firebase.initializeApp({
  apiKey: 'AIzaSyBSbWyyYLS5zXcY8Wv9oyI6nAkeC4q6R3s',
  authDomain: 'hog-engine-f691d.firebaseapp.com',
  databaseURL: 'https://hog-engine-f691d.firebaseio.com',
  projectId: 'hog-engine-f691d',
  storageBucket: 'hog-engine-f691d.appspot.com',
  messagingSenderId: '538483933560'
})
router.beforeEach((to, from, next) => {
  if (!store.state.stages.length) {
    store.dispatch('getStages').then(() => { next() })
  } else {
    next()
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  i18n,
  store,
  router,
  components: { App },
  template: '<App/>'
})
