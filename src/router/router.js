import Vue from 'vue'
import Router from 'vue-router'
import Editor from '../components/Editor.vue'
import Player from '../components/Player.vue'

Vue.use(Router)

const isAdmin = true // TODO

export default new Router({
  routes: isAdmin
    ? [
      { path: '/editor', component: Editor, name: 'editor' },
      { path: '/player/:stageId', component: Player, name: 'player' },
      { path: '/*', redirect: '/editor' }
    ]
    : []

})
