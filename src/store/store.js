import Vue from 'vue'
import Vuex from 'vuex'
import firebase from 'firebase'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    stages: [],
    sprites: [],
    loading: false,
    selectedStageId: null,
    pageTitle: '',
    spritesDroppedInStage: [],
    selectedSpriteId: null,
    fallbackMusic: 'https://firebasestorage.googleapis.com/v0/b/hog-engine-f691d.appspot.com/o/sounds%2FGlavni.mp3?alt=media&token=aecd07fe-a729-4ca7-a8bf-aced96ceb0f0'
  },
  mutations: {
    setStages (state, payload) {
      let stages = []
      for (const stageId in payload) {
        stages.push({
          ...payload[stageId],
          stageId
        })
      }
      state.stages = stages
    },
    addStage (state, payload) {
      state.stages.push({ ...payload })
    },
    setActiveStage (state, payload) {
      state.selectedStageId = payload
    },
    addSprite (state, payload) {
      state.sprites.push(payload)
    },
    removeSprite (state, spriteId) {
      state.sprites = state.sprites
        .filter(sprite => sprite.spriteId !== spriteId)
    },
    removeSprites (state) {
      state.sprites = []
    },
    setPageTitle (state, payload) {
      state.pageTitle = payload
    },
    setStageSprites (state, payload) {
      let sprites = []
      for (const spriteId in payload) {
        sprites.push({
          ...payload[spriteId],
          spriteId
        })
      }
      state.sprites = sprites
    },
    addSpriteToStage (state, spriteId) {
      state.spritesDroppedInStage.push(spriteId)
    },
    removeSpriteFromStage (state, spriteId) {
      state.spritesDroppedInStage = state.spritesDroppedInStage
        .filter(id => id !== spriteId)
    },
    clearSpritesInStage (state) {
      state.spritesDroppedInStage = []
    },
    updateSpritePosition (state, payload) {
      let sprite = state.sprites.find(item => item.spriteId === payload.spriteId)
      if (sprite) {
        sprite.position = payload.position
      }
    },
    updateSpriteOrder (state, payload) {
      let sprite = state.sprites.find(item => item.spriteId === payload.spriteId)
      if (sprite) {
        sprite.order = payload.order
      }
    },
    loadingOn (state) {
      state.loading = true
    },
    loadingOff (state) {
      state.loading = false
    },
    setLoading (state, payload) {
      state.loading = payload
    },
    setSeletedSpriteId (state, spriteId) {
      state.selectedSpriteId = spriteId
    }
  },
  actions: {
    getStages ({ commit }) {
      return new Promise((resolve, reject) => {
        firebase.database().ref('stages').once('value')
          .then(data => {
            commit('setStages', data.val())
            resolve()
          })
      })
    },
    addStage ({ commit }, payload) {
      firebase.database().ref('stages')
        .push({
          background: payload.background,
          name: payload.name,
          thumbnail: payload.thumbnail,
          sound: payload.sound
        })
        .then(data => {
          commit('addStage', {
            stageId: data.key,
            background: payload.background,
            name: payload.name,
            thumbnail: payload.thumbnail,
            sound: payload.sound
          })
        })
    },
    addSprite ({ commit }, payload) {
      firebase.database().ref(`stages/${payload.stageId}/sprites`)
        .push({
          position: payload.position,
          image: payload.image,
          name: payload.name,
          path: payload.path,
          order: 1
        })
        .then(data => {
          commit('addSprite', {
            image: payload.image,
            spriteId: data.key,
            stageId: payload.stageId,
            position: payload.position,
            name: payload.name,
            path: payload.path,
            order: 1
          })
        })
    },
    deleteSprite ({ commit }, sprite) {
      commit('loadingOn')
      firebase.database().ref(`stages/${sprite.stageId}sprites/${sprite.spriteId}`)
        .remove()
        .then(() => {
          return firebase.storage().ref(sprite.path).delete()
        })
        .then(() => {
          commit('removeSpriteFromStage', sprite.spriteId)
          commit('removeSprite', sprite.spriteId)
          commit('loadingOff')
        })
    },
    updateSpritePosition ({ commit }, { position, spriteId, stageId }) {
      let update = {}
      update[`stages/${stageId}/sprites/${spriteId}/position`] = position
      firebase.database().ref().update(update)
        .then(() => {
          commit('updateSpritePosition', { spriteId, position })
        })
    },
    updateSpriteOrder ({ commit }, { order, spriteId, stageId }) {
      return new Promise((resolve, reject) => {
        let update = {}
        update[`stages/${stageId}/sprites/${spriteId}/order`] = order
        firebase.database().ref().update(update)
          .then(() => {
            commit('updateSpriteOrder', { spriteId, order })
            resolve()
          })
      })
    }
  },
  getters: {
    loading (state) {
      return state.loading
    },
    stages (state) {
      return state.stages
    },
    stagesById (state) {
      let data = {}
      state.stages.forEach(item => {
        data[item.stageId] = item
      })
      return data
    },
    selectedStageId (state) {
      return state.selectedStageId
    },
    sprites (state) {
      return state.sprites
    },
    spritesDroppedInStage (state) {
      return state.spritesDroppedInStage
    },
    selectedSpriteId (state) {
      return state.selectedSpriteId
    },
    fallbackMusic (state) {
      return state.fallbackMusic
    }
  }
})
